#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h>


/*
int funtzioa(int dim, double *xptr, double *fptr)
{

		*(fptr) =*(xptr)*(*(xptr))-476123423434534535;
		return 1;
}

int jacobitarra(int dim, double *xptr, double *dptr)
{

		*(dptr) = 2*(*(xptr));
		return 1;

}
*/
/*

int funtzioa(int dim, double *xptr, double *fptr)
{

		*(fptr) = (-1) + (*(xptr)*(*(xptr))) + (*(xptr+1)*(*(xptr+1))) - (cos(*(xptr)))*(sin(*(xptr+1)));
		*(fptr+1) =*(xptr)*(*(xptr+1)) - (cos(*(xptr))) - (sin(*(xptr+1)));
		return 1;
}

int jacobitarra(int dim, double *xptr, double *dptr)
{

		*(dptr) = 2*(*(xptr)) + sin(*(xptr))*sin(*(xptr+1));
		*(dptr+1) = 2*(*(xptr+1)) - cos(*(xptr))*cos(*(xptr+1));
		*(dptr+2) = (*(xptr+1)) - (cos(*(xptr)));
		*(dptr+3) = (*(xptr)) + (sin(*(xptr+1)));
		return 1;

}*/


void f(int dim ,double *x,double *fx){

	//1 dim
	//fx=*fx * *fx +4;
	//2 dim
	fx[0]=-1+pow(x[0],2)+pow(x[1],2)-cos(x[0])*sin(x[1]);
	fx[1]=x[0]*x[1]-cos(x[1])-sin(x[0]);
	//3 dim
//	fx[0]=-1+pow(x[0],2)+pow(x[1],2)+pow(x[2],2);
//	fx[1]=pow(x[0],3)+pow(x[1],2)-pow(x[2],5);
//	fx[2]=-2+2*pow(x[0],2)+pow(-1+x[1],2)+pow(x[2],3);
}


void jakobiarra(int dim,double *x,double *jx){

	//1 dim
	//*jx = 2 * *jx;
	//2 dim
	jx[0]=2*x[0]+sin(x[0])*sin(x[1]); jx[1]=2*x[1]-cos(x[0])*cos(x[1]);
	jx[2]=x[1]-cos(x[0]);		  jx[3]=x[0]+sin(x[1]);
	//3 dim
//	jx[0]=2*x[0];		jx[1]=2*x[1];		jx[2]=2*x[2];
//	jx[3]=3*pow(x[0],2);	jx[4]=2*x[1];		jx[5]=-5*pow(x[2],4);
//	jx[6]=4*x[0];		jx[7]=2*(-1+x[1]);	jx[8]=3*pow(x[2],2);
	
}

