
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_linalg.h>

#define ZENB_HANDIA 13000

extern int datuak_lortu(char* path, int *dptr, double **pptrptr, double *tptr);
extern void f(int dim ,double *x,double *fx);
extern void jakobiarra(int dim,double *x,double *jx);

int main(int argc, char **argv)
{

	printf("\n");
 
  	char  path[15] = "sarrera.txt";
	int dim;
	double *XoPtr;
	double tolerantzia = 0.0001; //defetuzko tolerantzia
	int erroreaTolerantziaBainoTxikiago = 1;

	double *jac;
	double *f_x;
	jac = (double *)malloc(dim*dim*sizeof(double));//memorian tokia erreserbatu.
	f_x = malloc(dim*sizeof(double));


	if(datuak_lortu(path, &dim, &XoPtr, &tolerantzia)==-1){//sarrera.txt fitxategitik datuak lortu.

		printf("Datuak ez dira ondo irakurri fitxategitik\n");
		return -1;
	}

	printf("/////////////////////////////////////////////\n");
	printf("///////////////// FIND ROOT /////////////////\n");
	printf("/////////////////////////////////////////////\n");
	printf("\n");
	printf("Dimentsioa: %d\n", dim);
	printf("tolerantzia: %.e\n", tolerantzia);
	printf("Hasierako Hurbilpenak:\n");
	printf("\n");
	inprimatu(XoPtr, dim);
	printf("/////////////////////////////////////////////\n");
	printf("\n");

	int erantzunZuzena = newtonRaphson(dim, tolerantzia, XoPtr, jac, f_x, &erroreaTolerantziaBainoTxikiago);

	if(erantzunZuzena == 1)
	{
		printf("\n");
		printf("/////////////////////////////////////////////\n");
		printf("////////////// ERANTZUNA ////////////////////\n");
		printf("/////////////////////////////////////////////\n");
		printf("\n");
		//inprimatu(XoPtr, dim);
		inprimatu2(XoPtr, dim);
		printf("***** Hau soluzio posible bat da, ez du zertan bakarra izan *****\n");
		if(erroreaTolerantziaBainoTxikiago==1)printf("***** Soluzioaren errorea < %.e (norma euklidestarraren bidez kalkulatuta) *****\n", tolerantzia);
		else printf("***** Soluzioaren errorea > %.e (norma euklidestarraren bidez kalkulatuta) *****\n", tolerantzia);
		printf("\n");
	}
	else
	{

		printf("\n");
		printf("/////////////////////////////////////////////////////\n");
		printf("//// Aplikazioa ez da gai izan sistema ebazteko ////\n");
		printf("////////////////////////////////////////////////////\n");
		printf("\n");
		printf("**** Zer gertatu da?\n");
		printf("\n");
		printf("       1) Aukeratutako hasierako balioekin, algoritmoa ez da gai izan konbergentzia eremuan sartzeko\n");
		printf("       2) Sistemak ez du soluziorik\n");
		printf("\n");
		printf("**** Zer egin dezakezu?\n");
		printf("\n");
		printf("       1) Hasierako beste balio batzuekin berriro saiatu\n");
		printf("\n");

	}

}

int newtonRaphson(int dim, double tolerantzia, double *XoPtr, double *jac, double *f_x, int *erroreaTolerantziaBainoTxikiago)
{
	double norma = 0;
	double normaLag = ZENB_HANDIA;
	int normaObetuKop = 0;
	int normaTxartuKop = 0;
	int i = 0;
	int j = 0;
	int v = 0;
	int w = 0;
	int iterazioGehiago = ZENB_HANDIA;
	int iterazioGehiago2 = ZENB_HANDIA;
	int obetzen = 0;
	int lehenAldia = 1;
	int lehenAldia2 = 1;
	int erantzunZuzena=1;
	char string[20] = "";

	do
	{
		
		f(dim, XoPtr, f_x);
		jakobiarra(dim, XoPtr, jac);
		negatiboraPasa(dim, f_x);

		gsl_matrix_view m = gsl_matrix_view_array (jac, dim, dim);//GSL liburutegia erabiltzeko behar diren egiturak lortu.
   	 	gsl_vector_view b = gsl_vector_view_array (f_x, dim);
   	 	gsl_vector *x1 = gsl_vector_alloc (dim);

		int s;
  		gsl_permutation * p = gsl_permutation_alloc (dim);//Ekuazioak askatu
  		gsl_linalg_LU_decomp (&m.matrix, p, &s);
 	    gsl_linalg_LU_solve (&m.matrix, p, &b.vector, x1);

		for (j = 0; j < dim; ++j)//lortutako emaitzak gure bektorera kopiatu.
		{
			double lag = gsl_vector_get (x1, j);
			*(XoPtr+j) = lag + (*(XoPtr+j));
		}

		printf("%d. hurbilpena:\n", i+1);
		printf("\n");
		inprimatu(XoPtr, dim);//iterazio horretan lortutako hurbilpenak inprimatu

		normaEuklidestarra(dim, f_x, &norma);

		gsl_permutation_free (p);//GSL-ko bektoreak husteko.
 		gsl_vector_free (x1);


 		if(normaLag < norma)//norma obetu da azken iterazioan
 		{
 			normaTxartuKop++;
 			obetzen=0;
 		}
 		else //norma txartu da azken iterazioan.
 		{
 			normaObetuKop++;
 			obetzen++;
 		}
 		normaLag = norma;

 		if(zenbakiHandiegiak(dim,XoPtr))//double batean sartzen ez diren zenbakietara iritsi aurretik, dibergitzen hari delako, aplikazioa amaitzeko.
 		{
 			erantzunZuzena = 0;//Soluzio posiblerik ez da aurkitu
 			break;
 		}

 		if((normaObetuKop < (3*i)/4 && lehenAldia==1 && obetzen<5 && i>=50) || (iterazioGehiago < w))
 		{
 			lehenAldia = 0;
 			printf("---------------------------------------------------------------\n");
 			printf("-----Norma zenbat aldiz obetu da? %d\n", normaObetuKop);
 			printf("-----Norma zenbat aldiz txartu da? %d\n", normaTxartuKop);
 			printf("-----Azkeneko zenbat iteraziotan obetu da norma? %d\n", obetzen);
 			printf("-----------------------------------------------------------------------------\n");
 			printf("---- %d iteraziotan, konbergentzi eremutik kanpo dagoela ematen du----------\n", i);
 			printf("---- iteratzen jarraitu nahi duzu (bai/ez)?----------------------------------\n");
 			printf("-----------------------------------------------------------------------------\n");
 			scanf("%s", string);

 			if (!strncmp(string, "bai", 3))//iteratzen jarraitzeko
 			{
 				printf("---------------------------------------------------------------\n");
 				printf("---- Zenbat iterazio gehiago egin nahi dituzu?-----------------\n");
 				printf("---------------------------------------------------------------\n");
 				scanf("%s", string);
 				printf("\n");
 				iterazioGehiago = atoi(string);
 				w=0;

 			}
 			else//iteratzen ez jarraitu
 			{
 				erantzunZuzena = 0;//Soluzio posiblerik ez da aurkitu
 				break;
 			}
 		}
 		if((i>=15 && normaObetuKop > (3*i)/4 && obetzen > 5 && lehenAldia2==1) || iterazioGehiago2 < v)
 		{
 			lehenAldia2 = 0;
 			printf("----------------------------------------------------------------------------------------\n");
 			printf("-----Norma zenbat aldiz obetu da? %d\n", normaObetuKop);
 			printf("-----Norma zenbat aldiz txartu da? %d\n", normaTxartuKop);
 			printf("-----Azkeneko zenbat iteraziotan obetu da norma? %d\n", obetzen);
 			printf("----------------------------------------------------------------------------------------\n");
 			printf("---- %d iteraziotan ez du lortu emaitzaren errorea, tolerantzia baino txikiagoa izatea--\n", i);
 			printf("---- iterazioz iterazio emaitzatik gertuago dagoela dirudi ordea------------------------\n");
 			printf("-----jarraitu nahi duzu (bai/ez)?-------------------------------------------------------\n");
 			printf("----------------------------------------------------------------------------------------\n");
 			scanf("%s", string);

 			if (!strncmp(string, "bai", 3))//iteratzen jarraitzeko
 			{
 					
 				printf("---------------------------------------------------------------\n");
 				printf("---- Zenbat iterazio gehiago egin nahi dituzu?-----------------\n");
 				printf("---------------------------------------------------------------\n");
 				scanf("%s", string);
 				printf("\n");
 				iterazioGehiago2 = atoi(string);
 				v=0;

 			}
 			else//iteratzen ez jarraitu
 			{
 				erantzunZuzena = 1;//Soluzio posible bat aurkitu da, nahiz eta emaitzaren errorea tolerantzia baino handiagoa izan.
 				*erroreaTolerantziaBainoTxikiago = 0;
 				break;
 			}
 		}

 		i++;
 		v++;
 		w++;

 	} while(norma > tolerantzia && i<1000);

	if(erantzunZuzena == 1)return 1;
	else return 0;
	
}

int inprimatu(double *bek, int n)
{
	int i;
	for (i= 0; i < n; ++i)
		printf("      x%d = %.13f\n", i, bek[i]);
	printf("\n");
}

int inprimatu2(double *bek, int n)//esponentzialekin inprimatzeko.
{
	int i;
	for (i= 0; i < n; ++i)
		printf("      x%d = %.13e\n", i, bek[i]);
	printf("\n");
}

int normaEuklidestarra(int dim, double *f, double *norma)
{
	double batura = 0.0;
	int i = 0;

	for (i = 0; i < dim; i++)
	{
		batura += (*(f+i))*(*(f+i));
	}
	*norma = sqrt(batura);
	return 1;

}

int negatiboraPasa(int dim, double *f_x)
{
	int i;
	for (i = 0; i < dim; ++i)
	{
		*(f_x+i) = (*(f_x+i))*(-1);
	}

}

int zenbakiHandiegiak(int dim, double *bek)
{
	int i;
	for (i = 0; i < dim; ++i)
		if(*(bek+i)>0 && *(bek+i)>9999999999999999)return 1;//bektorean zenbaki handiegiren bat edo gehiago daude algoritmoarekin aurrera jarraitzeko.
		else if (*(bek+i)<0 && *(bek+i)<-9999999999999999)return 1;
	return 0;

}



